from django.db import models
from django.utils import timezone


# Create your models here.

class Property(models.Model):
    PropertyID = models.CharField(max_length=200)
    Address = models.CharField(max_length=250)
    SuburbName = models.CharField(max_length=200)
    Postcode = models.CharField(max_length=20)
    ListedDate = models.DateTimeField(null=True, blank=True)
    Price = models.CharField(max_length=200)
    SoldDate = models.DateTimeField(null=True, blank=True)
    SoldPrice = models.CharField(max_length=200, null=True, blank=True)
    ListedAgent = models.CharField(max_length=200, null=True, blank=True)
    CreateDate = models.DateTimeField(default=timezone.now)
    ModifiedDate = models.DateTimeField(default=timezone.now)


    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

