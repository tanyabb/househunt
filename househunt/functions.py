from lxml import html
import requests
from django.utils import timezone


def scrapingdata(form):
    page = requests.get(form.cleaned_data['url'])
    tree = html.fromstring(page.content)  ### Start scraping data from html ###
    search_result = tree.xpath('//div[@id="resultsInfo"]/p/text()')  # How many house have found
    search_result = ', '.join(search_result)

    suburb = tree.xpath('//div[@id="searchResultsTbl"]//a[@class="suburbLink"]/text()')
    suburb_name = ', '.join(suburb)[:-10]

    postcode = ', '.join(suburb)[-4:]
    listed_date = timezone.now()
    sold_date = timezone.now()
    create_date = timezone.now()

    search_list = tree.xpath('//article/@id')  # list of properties id

    twodlist = []
    for i in range(0, 1):
        row = []
        for j in range(0, len(search_list)):
            row_id = j + 1
            property_id = search_list[j]

            address = tree.xpath('//article[@id="' + property_id + '" ]//a[@class="name"]/text()')
            address = ', '.join(address)

            price = tree.xpath('//article[@id="' + property_id + '" ]//p[@class="priceText"]/text()')
            price = ', '.join(price)
            sold_price = ""

            agent_name = tree.xpath('//article[@id="' + property_id + '" ]//img[@class="agent-photo"]/@title')
            agent_name = ', '.join(agent_name)

            row.append(
                [search_result, row_id, property_id, address, suburb_name, postcode, listed_date, price, sold_date,
                 sold_price, agent_name, create_date])
        twodlist.append(row)
    return twodlist
