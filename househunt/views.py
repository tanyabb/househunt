from django.shortcuts import render
from .models import Property
from .forms import ScrapingForm, SearchForm
from django.utils import timezone
from .functions import scrapingdata
from django.core.cache import cache
from django.contrib import messages
import requests


# Create your views here.
def housescraping(request):
    form = ScrapingForm()

    if request.method == "POST":
        form = ScrapingForm(request.POST)
        if '_btnget' in request.POST:
            if form.is_valid():
                result = scrapingdata(form)
                search_result = result[0][0][0]
                cache.set('s1', result, 5)  # ('Key', 'Value', 'Timeout')
                return render(request, 'househunt/property_list.html',
                              {'form': form, 'list': result, 'search_result': search_result})
            else:
                return render(request, 'househunt/property_scraping.html', {'form': form})

        elif '_btnsave' in request.POST:
            result = cache.get('s1')
            if result:
                for row in result:
                    for i in range(0, len(row)):
                        prop_id = row[i][2]
                        sur_name = row[i][4]
                        postcode = row[i][5]
                        properties = Property.objects.filter(PropertyID=prop_id, SuburbName=sur_name, Postcode=postcode)
                        if not properties:
                            p = Property(
                                PropertyID=row[i][2],
                                Address=row[i][3],
                                SuburbName=row[i][4],
                                Postcode=row[i][5],
                                ListedDate=timezone.now(),
                                Price=row[i][7],
                                SoldDate=timezone.now(),
                                SoldPrice=row[i][9],
                                ListedAgent=row[i][10],
                                CreateDate=timezone.now(),
                                ModifiedDate=timezone.now(),
                            )
                            p.save()
                        else:
                            p = Property(
                                PropertyID=row[i][2],
                                Address=row[i][3],
                                SuburbName=row[i][4],
                                Postcode=row[i][5],
                                ListedDate=timezone.now(),
                                Price=row[i][7],
                                SoldDate=timezone.now(),
                                SoldPrice=row[i][9],
                                ListedAgent=row[i][10],
                                CreateDate=timezone.now(),
                                ModifiedDate=timezone.now(),
                            )
                            p._do_update()



                form = ScrapingForm()
                messages.success(request, "Save data successfully")
                return render(request, 'househunt/property_scraping.html', {'form': form}, )

    return render(request, 'househunt/property_scraping.html', {'form': form})


def property_search(request):
    # houses = House.objects.filter(listed_date=timezone.now()).order_by('propertyID')
    form = SearchForm()
    if request.method == "GET":
        form = SearchForm(request.GET)

        if form.is_valid():
            properties = Property.objects.filter(Postcode=form.cleaned_data['postcode'])
            return render(request, 'househunt/property_searchlist.html', {'form': form, 'property': properties})
        else:
            return render(request, 'househunt/property_search.html', {'form': form, })

    return render(request, 'househunt/property_search.html', {'form': form, })
