from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.housescraping, name='housescraping'),
    url(r'^househunt/$', views.property_search, name='property_search'),

]
